# README #

### Programming language ###
The chosen programming language was Java (version 8, release 131) because that is the language I'm most used to.

### Requirements ###
* Java 8 (release 131 or greater)
* Eclipse IDE (version Oxygen 2)

### Building the code ###
The code was developed on Eclipse IDE (Oxygen 2) and can be easily imported by walking through the following steps:

Obs.: Preferably, clone the project in your workspace folder.

* Click with the right button of the mouse inside the Project Explorer and select 'Import > Import...' (or) On the main menu, select 'File > Import...';
* Select the option 'General > Projects from Folder or Archive' and click in 'Next';
* Select the directory where the project was cloned to by click the button 'Directory...';
* Click on 'Finish'. The project will be imported.
* Click on the project folder with the right button of the mouse and select 'Properties';
* Go to 'Java Build Path' and, on the 'Libraries' tab, click on 'Add Library...';
* Select JUnit and click on 'Next'. Make sure "JUnit 5" on 'JUnit library version' is selected and click on 'Finish';
* Click on 'Apply';
* Under the 'Source' tab, click on 'Add Folder...';
* Check the folder 'src/main/resources' and click on 'OK';
* Click on 'Apply and Close';

The project is now ready to be executed.

### Executing the code ###
The file 'src/main/resources/input.txt' has the list of entries to be evaluated by the program. So, start by modifying that if you want to test other inputs.

To execute the code, one must run the class com.b2w.fas5.main.EntryPoint. After a successfull execution, the program will generate an output file (output.txt) inside its root folder. Refreshing the project on Eclipse IDE will make the file appear on the Project Explorer for easy access.

To execute the tests, one can run the Test Suite src.test.java.com.b2w.fas5.main.AllTests or the Test Cases individually.