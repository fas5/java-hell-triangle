/*
 *
 * HellTriangle
 *
 * EntryPoint
 */

package com.b2w.fas5.main;

import java.util.ArrayList;
import java.util.List;

import com.b2w.fas5.bst.Node;
import com.b2w.fas5.bst.BinarySearchTree;
import com.b2w.fas5.core.Builder;

/**
 * Entry point for program execution.
 *
 * @author Felipe de Assis Souza
 */
public class EntryPoint {

    public static void main(String[] args) {

        Builder builder = new Builder();

        String triangleData;

        while ((triangleData = builder.getInput()) != null) {
            List<Node[]> lista = builder.buildTriangleList(triangleData);

            BinarySearchTree tree = builder.buildTree(lista);

            tree.calculateMaxSum(tree.getRoot(), new ArrayList<>(), 0);

            builder.printOutput(formatSolution(tree));
        }

        builder.exit();
    }

    /* ------------------------ UTIL ------------------------ */

    private static String formatSolution(BinarySearchTree tree) {
        StringBuilder sb = new StringBuilder();

        for (Integer i : tree.getPath()) {
            sb.append(i).append(" + ");
        }

        sb.replace(sb.lastIndexOf("+") - 1, sb.lastIndexOf("+") + 2, " = ");
        sb.append(tree.getMaxTotal());

        return sb.toString();
    }

}
