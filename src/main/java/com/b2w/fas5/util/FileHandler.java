/*
 *
 * HellTriangle
 *
 * FileHandler
 */

package com.b2w.fas5.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A Singleton responsible for reading and writing in a text file.
 *
 * @author Felipe de Assis Souza
 */
public class FileHandler {

    private static final Logger logger = Logger.getLogger(FileHandler.class.getName());

    private static final String PROJECT_PATH = System.getProperty("user.dir");

    private static final String INPUT_FILE_NAME = "input.txt";

    private static final String OUTPUT_FILE_NAME = "output.txt";

    private static final String OUTPUT_FILE_PATH = PROJECT_PATH + File.separator + OUTPUT_FILE_NAME;

    private static final String CHARSET = "UTF-8";

    private static final boolean APPEND = false;

    private static final boolean AUTO_FLUSH = true;

    private static FileHandler fileHandler;

    private static BufferedReader bufferedReader;

    private static PrintWriter printWriter;

    static {
        try {
            // Reader
            ClassLoader classLoader = FileHandler.class.getClassLoader();
            File inputFile = new File(classLoader.getResource(INPUT_FILE_NAME).getFile());
            bufferedReader = new BufferedReader(new FileReader(inputFile));

            // Writer
            File outputFile = new File(OUTPUT_FILE_PATH);
            FileOutputStream fileOutputStream = new FileOutputStream(outputFile, APPEND);
            OutputStreamWriter ouputStreamWriter = new OutputStreamWriter(fileOutputStream, CHARSET);
            BufferedWriter bufferedWriter = new BufferedWriter(ouputStreamWriter);
            printWriter = new PrintWriter(bufferedWriter, AUTO_FLUSH);
        } catch (UnsupportedEncodingException | FileNotFoundException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    private FileHandler() {
    }

    /**
     * Creates and returns a new instance of the class <code>FileHandler</code>, in case it does not exist yet.
     * 
     * @return An instance of the class <code>FileHandler</code>.
     */
    public static FileHandler getInstance() {
        if (fileHandler == null) {
            fileHandler = new FileHandler();
        }

        return fileHandler;
    }

    /**
     * Reads a line from the input file.
     * 
     * @return A line from the input file.
     */
    public String read() {
        String line = null;

        try {
            line = bufferedReader.readLine();
        } catch (IOException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }

        return line;
    }

    /**
     * Writes the content passed as parameter.
     * 
     * @param text Content to be written.
     */
    public void write(String text) {
        printWriter.println(text);
        printWriter.flush();
    }

    /**
     * Closes the streams.
     */
    public void close() {
        closeReader();
        closeWriter();
    }

    private void closeWriter() {
        printWriter.close();
    }

    private void closeReader() {
        try {
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
