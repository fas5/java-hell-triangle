/*
 *
 * HellTriangle
 *
 * Tree
 */

package com.b2w.fas5.bst;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a Binary Search Tree.
 *
 * @author Felipe de Assis Souza
 */
public class BinarySearchTree {

    private Node root;

    private List<Integer> path;

    private Integer maxTotal;

    /**
     * Empty constructor.
     */
    public BinarySearchTree() {
    }

    /**
     * Default constructor.
     * 
     * @param root Starting node of this tree.
     */
    public BinarySearchTree(Node root) {
        this.root = root;
        this.maxTotal = 0;
    }

    /**
     * Recursively walks through the BST to find the maximum total from top to bottom.
     * 
     * @param node Node being visited.
     * @param path The path traveled in the BST.
     * @param total Total summed according to the path traveled.
     */
    public void calculateMaxSum(Node node, List<Integer> path, Integer total) {
        if (node == null) {
            if (total > this.maxTotal) {
                this.path = new ArrayList<>(path);
                this.maxTotal = total;
            }
        } else {
            path.add(node.getValue());
            total += node.getValue();

            calculateMaxSum(node.getLeft(), path, total);

            if (node.getLeft() != null) {
                path.remove(path.size() - 1);
            }

            calculateMaxSum(node.getRight(), path, total);

            if (node.getRight() != null) {
                path.remove(path.size() - 1);
            }
        }
    }

    /**
     * Lists the values of all nodes that compose the tree, in preorder.
     * 
     * @param node Node being visited.
     * @return List of the values of all nodes that compose the tree.
     */
    public List<Integer> listNodes(Node node) {
        List<Integer> nodes = new ArrayList<>();

        if (node == null) {
            return nodes;
        } else {
            walkThroughTree(node.getLeft(), nodes);
            walkThroughTree(node.getRight(), nodes);
        }

        return nodes;
    }

    private void walkThroughTree(Node node, List<Integer> nodes) {
        if (node == null) {
            return;
        } else {
            nodes.add(node.getValue());

            walkThroughTree(node.getLeft(), nodes);

            walkThroughTree(node.getRight(), nodes);
        }
    }

    /**
     * Retorna o valor do campo <CODE>root</CODE> deste objeto.
     *
     * @return O valor do campo <CODE>root</CODE> deste objeto.
     */
    public Node getRoot() {
        return root;
    }

    /**
     * Seta o valor do campo root deste objeto.
     *
     * @param root valor do campo root
     */
    public void setRoot(Node root) {
        this.root = root;
    }

    /**
     * Retorna o valor do campo <CODE>path</CODE> deste objeto.
     *
     * @return O valor do campo <CODE>path</CODE> deste objeto.
     */
    public List<Integer> getPath() {
        return path;
    }

    /**
     * Seta o valor do campo path deste objeto.
     *
     * @param path valor do campo path
     */
    public void setPath(List<Integer> path) {
        this.path = path;
    }

    /**
     * Retorna o valor do campo <CODE>maxTotal</CODE> deste objeto.
     *
     * @return O valor do campo <CODE>maxTotal</CODE> deste objeto.
     */
    public Integer getMaxTotal() {
        return maxTotal;
    }

    /**
     * Seta o valor do campo maxTotal deste objeto.
     *
     * @param maxTotal valor do campo maxTotal
     */
    public void setMaxTotal(Integer maxTotal) {
        this.maxTotal = maxTotal;
    }

}
