/*
 *
 * HellTriangle
 *
 * Node
 */

package com.b2w.fas5.bst;

/**
 * Represents a node of a Binary Search Tree.
 *
 * @author Felipe de Assis Souza
 */
public class Node {

    private Integer value;

    private Node left;

    private Node right;

    /**
     * Default constructor.
     * 
     * @param value Value stored in this node.
     */
    public Node(Integer value) {
        this.value = value;
        this.left = null;
        this.right = null;
    }

    /**
     * Constructor.
     * 
     * @param value Value stored in this node.
     * @param left Left child.
     * @param right RIght child.
     */
    public Node(Integer value, Node left, Node right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    /**
     * Retorna o valor do campo <CODE>value</CODE> deste objeto.
     *
     * @return O valor do campo <CODE>value</CODE> deste objeto.
     */
    public Integer getValue() {
        return value;
    }

    /**
     * Seta o valor do campo value deste objeto.
     *
     * @param value valor do campo value
     */
    public void setValue(Integer value) {
        this.value = value;
    }

    /**
     * Retorna o valor do campo <CODE>left</CODE> deste objeto.
     *
     * @return O valor do campo <CODE>left</CODE> deste objeto.
     */
    public Node getLeft() {
        return left;
    }

    /**
     * Seta o valor do campo left deste objeto.
     *
     * @param left valor do campo left
     */
    public void setLeft(Node left) {
        this.left = left;
    }

    /**
     * Retorna o valor do campo <CODE>right</CODE> deste objeto.
     *
     * @return O valor do campo <CODE>right</CODE> deste objeto.
     */
    public Node getRight() {
        return right;
    }

    /**
     * Seta o valor do campo right deste objeto.
     *
     * @param right valor do campo right
     */
    public void setRight(Node right) {
        this.right = right;
    }

}
