/*
 *
 * HellTriangle
 *
 * Builder
 */

package com.b2w.fas5.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.b2w.fas5.bst.Node;
import com.b2w.fas5.bst.BinarySearchTree;
import com.b2w.fas5.util.FileHandler;

/**
 * Responsible for interpreting the input given and build a data structure equivalent to it.
 *
 * @author Felipe de Assis Souza
 */
public class Builder {

    private static final String EMPTY_STRING = "";

    private static final String REGEX = "[^\\d]";

    private static FileHandler fileHandler;

    /**
     * Default constructor.
     */
    public Builder() {
        fileHandler = FileHandler.getInstance();
    }

    /**
     * Builds a {@link Node} array equivalent to the input given.
     * 
     * @param triangleData Input to be converted to an array of Nodes.
     * @return A list equivalent to the input given.
     */
    public List<Node[]> buildTriangleList(String triangleData) {
        final List<String> rawList = arrayToList(triangleData.split(REGEX));
        final int rawListSize = rawList.size();

        final List<Node[]> triangleList = new ArrayList<>();

        int counter = 0;
        for (int i = 1; i <= rawListSize && counter < rawListSize; i++) {
            final Node[] subArray = new Node[i];

            for (int j = 0; j < subArray.length && counter < rawListSize; j++) {
                subArray[j] = new Node(Integer.valueOf(rawList.get(counter)));
                counter++;
            }

            triangleList.add(subArray);
        }

        return triangleList;
    }

    /**
     * Builds a {@link BinarySearchTree} according to the list of {@link Node}s passed as parameter.
     * 
     * @param triangleList List of Nodes that will compose the Tree.
     * @return A Tree object corresponding to the list of Nodes.
     */
    public BinarySearchTree buildTree(List<Node[]> triangleList) {
        for (int i = 0; i < triangleList.size() - 1; i++) {
            Node[] subArray = triangleList.get(i);
            Node[] subArrayChildren = triangleList.get(i + 1);

            for (int j = 0; j < subArray.length; j++) {
                Node currentNode = subArray[j];
                currentNode.setLeft(subArrayChildren[j]);
                currentNode.setRight(subArrayChildren[j + 1]);
            }
        }

        return new BinarySearchTree(triangleList.get(0)[0]);
    }

    /**
     * Reads a line from the input file.
     * 
     * @return A line from the input file.
     */
    public String getInput() {
        return fileHandler.read();
    }

    /**
     * Prints the output of the algorithm in the output file.
     * 
     * @param output Content to be written.
     */
    public void printOutput(String output) {
        fileHandler.write(output);
    }

    /**
     * Releases any system resources associated with the program.
     */
    public void exit() {
        fileHandler.close();
    }

    /* ------------------------ UTIL ------------------------ */

    /**
     * Converts a String array in a String list, removing the empry Strings.
     * 
     * @param array Array to be converted.
     * @return A list of Strings containing all non-empty Strings.
     */
    private List<String> arrayToList(String[] array) {
        List<String> list = new ArrayList<>(Arrays.asList(array));

        list.removeAll(Arrays.asList(new String[] { EMPTY_STRING }));

        return list;
    }
}
