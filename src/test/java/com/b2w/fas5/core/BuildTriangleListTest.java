/*
 *
 * HellTriangle
 *
 * BuildTriangleListTest
 */

package com.b2w.fas5.core;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import com.b2w.fas5.bst.Node;
import com.b2w.fas5.common.CommonTest;

/**
 * Test case for building the list equivalent to the input.
 *
 * @author Felipe de Assis Souza
 */
public class BuildTriangleListTest extends CommonTest {

    @Test
    public void testBuildTriangleList() {
        boolean isEqual = true;

        List<Node[]> list = builder.buildTriangleList(INPUT);

        for (int i = 0; i < list.size() && isEqual; i++) {
            Node[] nodeArray = list.get(i);
            Node[] array = list.get(i);

            for (int j = 0; j < nodeArray.length && isEqual; j++) {
                if (!nodeArray[j].equals(array[j])) {
                    isEqual = false;
                }
            }
        }

        assertTrue(isEqual);
    }

}
