/*
 *
 * HellTriangle
 *
 * BuildTreeTest
 */

package com.b2w.fas5.core;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import com.b2w.fas5.bst.BinarySearchTree;
import com.b2w.fas5.common.CommonTest;

/**
 * Test case for building the tree equivalent to the Hell Triangle.
 *
 * @author Felipe de Assis Souza
 */
public class BuildTreeTest extends CommonTest {

    @Test
    public void testBuildTree() {
        BinarySearchTree tree = builder.buildTree(mockTreeNodesList);

        List<Integer> mockTreeNodesValues = mockTree.listNodes(mockTree.getRoot());
        List<Integer> treeNodesValues = tree.listNodes(tree.getRoot());

        assertTrue(treeNodesValues.equals(mockTreeNodesValues));
    }

}
