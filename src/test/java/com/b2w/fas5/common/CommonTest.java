/*
 *
 * HellTriangle
 *
 * CommonTest
 */

package com.b2w.fas5.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import com.b2w.fas5.bst.BinarySearchTree;
import com.b2w.fas5.bst.Node;
import com.b2w.fas5.core.Builder;

/**
 * Common class for all Test classes. Contains resources used by multiple test cases.
 *
 * @author Felipe de Assis Souza
 */
public class CommonTest {

    protected static final String INPUT = "[[6],[3,5],[9,7,1],[4,6,8,4]]";

    protected static List<Node[]> mockTriangleList = new ArrayList<>();

    protected static List<Node[]> mockTreeNodesList = new ArrayList<>();

    protected static BinarySearchTree mockTree = new BinarySearchTree();

    protected static Builder builder = new Builder();

    @BeforeClass
    public static void init() {
        Node nodeO = new Node(4);
        Node nodeN = new Node(8);
        Node nodeM = new Node(8);
        Node nodeL = new Node(6);
        Node nodeK = new Node(8);
        Node nodeJ = new Node(6);
        Node nodeI = new Node(6);
        Node nodeH = new Node(4);
        Node nodeG = new Node(1, nodeN, nodeO);
        Node nodeF = new Node(7, nodeL, nodeM);
        Node nodeE = new Node(7, nodeJ, nodeK);
        Node nodeD = new Node(9, nodeH, nodeI);
        Node nodeC = new Node(5, nodeF, nodeG);
        Node nodeB = new Node(3, nodeD, nodeE);
        Node nodeA = new Node(6, nodeB, nodeC);

        mockTriangleList.add(new Node[] { nodeA });
        mockTriangleList.add(new Node[] { nodeB, nodeC });
        mockTriangleList.add(new Node[] { nodeD, nodeE, nodeG });
        mockTriangleList.add(new Node[] { nodeH, nodeI, nodeK, nodeO });

        mockTreeNodesList.add(new Node[] { nodeA });
        mockTreeNodesList.add(new Node[] { nodeB, nodeC });
        mockTreeNodesList.add(new Node[] { nodeD, nodeE, nodeF, nodeG });
        mockTreeNodesList.add(new Node[] { nodeH, nodeI, nodeJ, nodeK, nodeL, nodeM, nodeN, nodeO });

        mockTree.setRoot(nodeA);
        mockTree.setPath(Arrays.asList(new Integer[] { 6, 5, 7, 8 }));
        mockTree.setMaxTotal(26);
    }

    @AfterClass
    public static void reset() {
        mockTriangleList.clear();
        mockTreeNodesList.clear();
    }

}
