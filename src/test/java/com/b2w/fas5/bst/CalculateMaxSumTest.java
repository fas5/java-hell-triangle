/*
 *
 * HellTriangle
 *
 * CalculateMaxSumTest
 */

package com.b2w.fas5.bst;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.b2w.fas5.common.CommonTest;

/**
 * Test case for calculating the max sum.
 *
 * @author Felipe de Assis Souza
 */
public class CalculateMaxSumTest extends CommonTest {

    private static List<Node[]> treeNodesList;

    @BeforeClass
    public static void beforeMethod() {
        treeNodesList = new ArrayList<>(mockTreeNodesList);
        for (int i = 0; i < treeNodesList.size(); i++) {
            Node nodes[] = treeNodesList.get(i);
            for (Node node : nodes) {
                node.setLeft(null);
                node.setRight(null);
            }
        }
    }

    @Test
    public void testCalculateMaxSum() {
        BinarySearchTree tree = builder.buildTree(treeNodesList);
        tree.calculateMaxSum(tree.getRoot(), new ArrayList<>(), 0);
        assertTrue(mockTree.getMaxTotal() == tree.getMaxTotal());
    }

}
