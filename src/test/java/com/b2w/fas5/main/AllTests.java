/*
 *
 * HellTriangle
 *
 * AllTests
 */

package com.b2w.fas5.main;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.b2w.fas5.bst.CalculateMaxSumTest;
import com.b2w.fas5.bst.MaxSumPathTest;
import com.b2w.fas5.core.BuildTreeTest;
import com.b2w.fas5.core.BuildTriangleListTest;

/**
 * Test suite for running all test cases.
 *
 * @author Felipe de Assis Souza
 */
@RunWith(Suite.class)
@SuiteClasses({ BuildTriangleListTest.class, BuildTreeTest.class, CalculateMaxSumTest.class, MaxSumPathTest.class })
public class AllTests {
}
